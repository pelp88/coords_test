package ru.codeinside.generics;

import java.util.ArrayList;
import java.util.List;

public class CoordinatesStorage<T extends Point> {
    private List<T> coords = new ArrayList<>();


    public List<T> getCoords() {
        return coords;
    }

    public void setCoords(List<T> coords) {
        this.coords = coords;
    }
}
