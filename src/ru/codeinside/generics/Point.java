package ru.codeinside.generics;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;

public abstract class Point<V extends Number> {
    private Integer dimension;
    private HashMap<String, V> coords = new HashMap<>();

    @SafeVarargs
    public Point(V... args) {
       Iterator<String> alphabet = Arrays.asList( "abcdefghijklmnopqrstuvwxyz").iterator();
        for (V val: args) {
            this.setCoordinate(alphabet.next(), val);
        }
        this.setDimension(args.length);
    }

    public void setCoordinate(String axis, V value) {
        coords.put(axis, value);
    }

    public V getCoordinate(String axis) {
        return coords.get(axis);
    }

    public Integer getDimension() {
        return this.dimension;
    }

    public void setDimension(Integer val) {
        this.dimension = val;
    }

    @Override
    public String toString() {
        return hashMapToString(this.coords);
    }

    private String hashMapToString(HashMap<?, ?> map) {
        return map.keySet().stream().map(key -> key + "=" + map.get(key))
                .collect(Collectors.joining(", ", "{", "}"));
    }
}
